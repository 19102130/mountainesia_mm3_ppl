<!-- Koneksi -->
<?php include("path.php"); ?>
<?php include(ROOT_PATH . "/app/controllers/users.php");
?>
<!DOCTYPE html>
<html style="font-size: 16px;">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Cetak kode Booking</title>
    <link rel="stylesheet" href="assets/css/nicepage.css" media="screen">
<link rel="stylesheet" href="assets/css/Cetak-kode-Booking.css" media="screen">
<link rel="stylesheet" href="cetak-kode-booking-print.css" media="screen">
    <script class="u-script" type="text/javascript" src="jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 3.29.1, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    
    
    <script type="application/ld+json">{
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": ""
}</script>
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Cetak kode Booking">
    <meta property="og:type" content="website">
  </head>
  <body class="u-body u-overlap u-overlap-transparent">

    <section class="u-clearfix u-image u-section-1" id="sec-d70d" data-image-width="1600" data-image-height="1063">
      <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-expanded-width-sm u-expanded-width-xs u-shape u-shape-rectangle u-white u-shape-1" data-animation-name="fadeIn" data-animation-duration="1000" data-animation-delay="0" data-animation-direction=""></div>
                          <?php 
                            require'app/database/connect.php';
                            $no =1;    
                            $query=mysqli_query($conn, "SELECT * FROM booking")or die(mysqli_error());
                            while($data=mysqli_fetch_array($query)): 
                           ?>
        <h3 class="u-text u-text-default u-text-1">Cetak Kode Booking</h3>
        <p class="u-align-left u-text u-text-grey-40 u-text-2">No</p>
        <p class="u-align-left u-text u-text-3"><?php echo $no; ?></p>
        <p class="u-align-left u-text u-text-grey-40 u-text-4">Ketua Kelompok</p>
        <h5 class="u-align-left u-text u-text-3"><?= $data['nama_ketua']?></h5>
         <p class="u-align-left u-text u-text-grey-40 u-text-4">No Telepon</p>
        <h5 class="u-align-left u-text u-text-5"><?= $data['no_telp']?></h5>
        <p class="u-align-left u-text u-text-grey-40 u-text-4">Nama Gunung</p>
        <h5 class="u-align-left u-text u-text-5"><?= $data['pilih_gunung']?></h5>
        <p class="u-align-left u-text u-text-grey-40 u-text-6">Jalur pendakian</p>
       <h5 class="u-align-left u-text u-text-5"><?= $data['pilih_jalur']?></h5>
        <p class="u-align-left u-text u-text-grey-40 u-text-8">Tanggal Pendakian</p>
        <h5 class="u-align-left u-text u-text-9"><?= $data['tgl_pendakian']?></h5>
        <p class="u-align-left u-text u-text-grey-40 u-text-8">Kelompok Pendakian</p>
        <h5 class="u-align-left u-text u-text-9"><?= $data['kelompok_pendakian']?></h5>
              <?php $no++; endwhile; ?>
        <p class="u-align-center u-small-text u-text u-text-grey-30 u-text-variant u-text-10">Bawa kertas ini ke basecamp tempat anda ingin mendaki</p>
      </div>
        <script>
        window.print();
    </script>
    </section>

  </body>
</html>
