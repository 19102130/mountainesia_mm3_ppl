-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2022 at 11:07 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyek-perangkat-lunak`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `nama_ketua` varchar(55) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `pilih_gunung` varchar(55) NOT NULL,
  `pilih_jalur` varchar(55) NOT NULL,
  `tgl_pendakian` date NOT NULL,
  `kelompok_pendakian` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `username`, `nama_ketua`, `no_telp`, `pilih_gunung`, `pilih_jalur`, `tgl_pendakian`, `kelompok_pendakian`) VALUES
(32, '', 'usama', '0812343212321', 'slamet', 'Guci', '2022-01-31', 'Sedang (4-9 orang)'),
(33, '', 'andi', '0982719361836', 'prau', 'Kalilembu', '2022-01-30', 'Besar (10 orang lebih)'),
(34, '', 'trias', '082693628138', 'sindoro', 'Sikatok', '2022-02-01', 'Kecil (1-3 orang)');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
